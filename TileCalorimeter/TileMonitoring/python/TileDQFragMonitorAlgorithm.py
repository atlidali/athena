#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

'''
@file TileDQFragMonitorAlgorithm.py
@brief Python configuration of TileDQFragMonitorAlgorithm algorithm for the Run III
'''


def TileDQFragMonitoringConfig(flags, **kwargs):

    ''' Function to configure TileDQFragMonitorAlgorithm algorithm in the monitoring system.'''


    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    result = ComponentAccumulator()

    from DetDescrCnvSvc.DetDescrCnvSvcConfig import DetDescrCnvSvcCfg
    result.merge(DetDescrCnvSvcCfg(flags))

    from TileConditions.TileCablingSvcConfig import TileCablingSvcCfg
    result.merge( TileCablingSvcCfg(flags) )

    from TileRecUtils.TileDQstatusConfig import TileDQstatusAlgCfg
    result.merge( TileDQstatusAlgCfg(flags) )

    if flags.Tile.useDCS:
        from TileConditions.TileDCSConfig import TileDCSCondAlgCfg
        result.merge( TileDCSCondAlgCfg(flags) )

    from TileConditions.TileBadChannelsConfig import TileBadChanToolCfg
    badChanTool = result.popToolsAndMerge( TileBadChanToolCfg(flags) )


    # The following class will make a sequence, configure algorithms, and link
    # them to GenericMonitoringTools
    from AthenaMonitoring import AthMonitorCfgHelper
    helper = AthMonitorCfgHelper(flags,'TileMonitoring')

    # Adding an TileDQFragMonitorAlgorithm algorithm to the helper
    from TileMonitoring.TileMonitoringConf import TileDQFragMonitorAlgorithm
    tileDQFragMonAlg = helper.addAlgorithm(TileDQFragMonitorAlgorithm, 'TileDQFragMonAlg')

    tileDQFragMonAlg.TileBadChanTool = badChanTool
    tileDQFragMonAlg.CheckDCS = flags.Tile.useDCS

    # 1) Configure histogram with TileDQFragMonAlg algorithm execution time
    executeTimeGroup = helper.addGroup(tileDQFragMonAlg, 'TileDQFragMonExecuteTime', 'Tile/')
    executeTimeGroup.defineHistogram('TIME_execute', path = 'DMUErrors', type='TH1F',
                                     title = 'Time for execute TileDQFragMonAlg algorithm;time [#mus]',
                                     xbins = 300, xmin = 0, xmax = 300000)

    # 2) Configure histogram with Tile error state in EventInfo vs lumi block
    errorStateGroup = helper.addGroup(tileDQFragMonAlg, 'TileEventsWithErrEventInfoLB', 'Tile/DMUErrors')
    errorStateGroup.defineHistogram('lumiBlock;TileEventsWithErrEventInfo', path = 'BadDrawers', type='TH1F',
                                    title = '# events with Tile error state in EventInfo;LumiBlock;# events with error',
                                    xbins = 1000, xmin = -0.5, xmax = 999.5)

    # 3) Configure histogram with TileDQFragMonAlg algorithm execution time
    consecutiveBadGroup = helper.addGroup(tileDQFragMonAlg, 'TileConsecutiveBadModules', 'Tile/DMUErrors')
    consecutiveBadGroup.defineHistogram('TileConsecutiveBad', path = 'BadDrawers', type='TH1F',
                                        title = 'Max # Tile consecutive bad modules;# consecutive bad modules;N',
                                        xbins = 17, xmin = -0.5, xmax = 16.5)

    # 4) Configure histogram with TileDQFragMonAlg algorithm execution time
    consecutiveBadLBGroup = helper.addGroup(tileDQFragMonAlg, 'TileConsecutiveBadModulesLB', 'Tile/DMUErrors')
    consecutiveBadLBGroup.defineHistogram('lumiBlock,TileConsecutiveBad;TileConsecutiveBadLB', path = 'BadDrawers', type='TH2F',
                                          title = 'Max # Tile consecutive bad modules;LumiBlock;# consecutive bad modules',
                                          xbins = 1000, xmin = -0.5, xmax = 999.5, ybins = 17, ymin = -0.5, ymax = 16.5)


    run = str(flags.Input.RunNumber[0])

    from TileMonitoring.TileMonitoringCfgHelper import getPartitionName
    from TileCalibBlobObjs.Classes import TileCalibUtils as Tile

    # 5) Configure histogram with mismatched L1 trigger type of Tile module
    modulePartitionLabels = [str(module) for module in range(1, Tile.MAX_DRAWER + 1)]
    modulePartitionLabels += [getPartitionName(ros) for ros in range(1, Tile.MAX_ROS)]

    mismatchedLVL1Group = helper.addGroup(tileDQFragMonAlg, 'TileMismatchedL1TiggerType', 'Tile/')
    mismatchedLVL1Group.defineHistogram('module,ROS;TileMismatchedL1TiggerType', path = 'DMUErrors',
                                        title = run + ': Tile mismatched L1 Trigger Type;Module;Partition',
                                        type = 'TH2F', labels = modulePartitionLabels,
                                        xbins = Tile.MAX_DRAWER, xmin = 0.0, xmax = Tile.MAX_DRAWER,
                                        ybins = Tile.MAX_ROS - 1, ymin = 1.0, ymax = Tile.MAX_ROS)


    # 6) Configure histogram with no all Tile digits in the case of Trigger Type = 0x82
    noAllDigitsGroup = helper.addGroup(tileDQFragMonAlg, 'TileNoAllDigits', 'Tile/')
    noAllDigitsGroup.defineHistogram('module,ROS;TileNoalldigits', path = 'DMUErrors', type = 'TH2F', labels = modulePartitionLabels,
                                     title = run + ': No All Tile digits in event with Trigger Type = 0x82;Module;Partition',
                                     xbins = Tile.MAX_DRAWER, xmin = 0.0, xmax = Tile.MAX_DRAWER,
                                     ybins = Tile.MAX_ROS - 1, ymin = 1.0, ymax = Tile.MAX_ROS)



    # 7) Configure histograms with Tile DMU errors
    maxDMUs = 16
    dmuErrorLabels = [str(dmu) for dmu in range(0, maxDMUs)]

    dmuErrorLabels += ['OK', 'HEADER_FORM', 'HEADER_PAR', 'MEMO_PAR', 'FE_CRC', 'ROD_CRC', 'BCID']
    dmuErrorLabels += ['SAMPLE_FORM', 'SAMPLE_PAR', 'DOUBLE_STB', 'SINGLE_STB', 'GLOBAL_CRC']
    dmuErrorLabels += ['DUMMY_FRAG', 'NO_RECO_FRAG', 'MASKED', 'ALL_M_BAD_DCS', 'ANY_CH_BAD_HV']

    dmuErrorLabels += ['0 -> 1023', 'Zeros', 'Two 1023 + ped', 'Jump 2 levels', 'Single Up + ped']
    dmuErrorLabels += ['Single Dn + ped', 'Single Up + sig', 'Single Dn + sig', 'Ped > 200 LG']
    dmuErrorLabels += ['Single Dn LG_s0', 'Single Dn LG_s6', 'Up LG_s0_s6 or Gap', 'Dn LG_s0_s6 or Gap']

    maxErrors = len(dmuErrorLabels) - maxDMUs

    errorsArray = helper.addArray([int(Tile.MAX_ROS - 1), int(Tile.MAX_DRAWER)],
                                  tileDQFragMonAlg, 'TileDigiErrors', topPath = 'Tile/')
    for postfix, tool in errorsArray.Tools.items():
        ros, module = [int(x) for x in postfix.split('_')[1:]]

        moduleName = Tile.getDrawerString(ros + 1, module)
        title = 'Run ' + run + ': ' + moduleName + ' DMU Header Errors;DMU'
        name = 'DMU,Error;TileDigiErrors' + moduleName

        tool.defineHistogram(name, title = title, type = 'TH2F',
                             labels = dmuErrorLabels, path = 'DMUErrors',
                             xbins = maxDMUs, xmin = 0.0, xmax = maxDMUs,
                             ybins = maxErrors, ymin = 0.0, ymax = maxErrors)



    # 8) Configure histograms with fraction of events/DMUs Tile DMU errors vs lumi blocks
    errorsVsLBArray = helper.addArray([int(Tile.MAX_ROS - 1), int(Tile.MAX_DRAWER)],
                                      tileDQFragMonAlg, 'FracTileDigiErrors', topPath = 'Tile/')
    for postfix, tool in errorsVsLBArray.Tools.items():
        ros, module = [int(x) for x in postfix.split('_')[1:]]

        moduleName = Tile.getDrawerString(ros + 1, module)
        title = 'Run ' + run + ': ' + moduleName + ' (#total_events - #ok_events)/(#total_events)'
        title += ';LumiBlock;Fraction of Digital errors'
        name = 'lumiBlock,fractionOfBadDMUs;FracTileDigiErrors' + moduleName

        tool.defineHistogram(name, title = title, path = 'DMUErrors', type = 'TProfile',
                             xbins = 1000, xmin = -0.5, xmax = 999.5)



    from TileMonitoring.TileMonitoringCfgHelper import addTilePartitionMapsArray

    # 9) Configure histograms with # of jumps errors per Tile partition
    addTilePartitionMapsArray(helper, tileDQFragMonAlg, path = 'Tile/DMUErrors/BadDrawers',
                              name = 'TileBadChannelsJumpMap', title = '# Jump errors',
                              run = run)

    # 10) Configure histograms with # of not masked jumps errors per Tile partition
    addTilePartitionMapsArray(helper, tileDQFragMonAlg, path = 'Tile/DMUErrors/BadDrawers',
                              name = 'TileBadChannelsJumpNotMaskMap', title = '# Not masked Jump errors',
                              run = run)


    tileDQFragMonAlg.TileRawChannelContainer = flags.Tile.RawChannelContainer

    # 11) Configure histograms with Tile bad pulse shape
    addTilePartitionMapsArray(helper, tileDQFragMonAlg, path = 'Tile/DMUErrors/BadDrawers',
                              name = 'TileBadPulseQualityMap', run = run,
                              title = 'Bad pulse shape or #chi^{2} from Optimal Filtering algirithm')

    # 12) Configure histograms with Tile channel negative amplitudes below threshold
    addTilePartitionMapsArray(helper, tileDQFragMonAlg, path = 'Tile/DMUErrors/BadDrawers',
                              name = 'TileBadChannelsNegMap', title = '# Negative amplitude',
                              run = run)

    # 13) Configure histograms with not masked Tile channel negative amplitudes below threshold
    addTilePartitionMapsArray(helper, tileDQFragMonAlg, path = 'Tile/DMUErrors/BadDrawers',
                              name = 'TileBadChannelsNegNotMaskMap', run = run,
                              title = '# Not masked negative amplitude')


    accumalator = helper.result()
    result.merge(accumalator)
    return result


if __name__=='__main__':

    # Setup the Run III behavior
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = True

    # Setup logs
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import INFO
    log.setLevel(INFO)

    # Set the Athena configuration flags
    from AthenaConfiguration.AllConfigFlags import ConfigFlags

    from AthenaConfiguration.TestDefaults import defaultTestFiles
    ConfigFlags.Input.Files = defaultTestFiles.RAW
    ConfigFlags.Output.HISTFileName = 'TileDQFragMonitorOutput.root'
    ConfigFlags.DQ.useTrigger = False
    ConfigFlags.DQ.enableLumiAccess = False
    ConfigFlags.Tile.doOptATLAS = True
    ConfigFlags.lock()

    # Initialize configuration object, add accumulator, merge, and run.
    from AthenaConfiguration.MainServicesConfig import MainServicesThreadedCfg
    cfg = MainServicesThreadedCfg(ConfigFlags)

    from ByteStreamCnvSvc.ByteStreamConfig import ByteStreamReadCfg
    tileTypeNames = ['TileRawChannelContainer/TileRawChannelCnt', 'TileDigitsContainer/TileDigitsCnt']
    cfg.merge( ByteStreamReadCfg(ConfigFlags, typeNames = tileTypeNames) )

    from TileRecUtils.TileRawChannelMakerConfig import TileRawChannelMakerCfg
    cfg.merge( TileRawChannelMakerCfg(ConfigFlags) )

    cfg.merge( TileDQFragMonitoringConfig(ConfigFlags) )

    cfg.printConfig(withDetails = True, summariseProps = True)

    cfg.store( open('TileDQFragMonitorAlgorithm.pkl','w') )

    sc = cfg.run(maxEvents = 3)

    import sys
    # Success should be 0
    sys.exit(not sc.isSuccess())

